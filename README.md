## Sobre o projeto

O código foi criado inicialmente utilizando a classe Robot que é a classe que têm os comandos mais básicos. Após dar uma olhada na documentação descobri a classe AdvancedRobot. Ela tem métodos em que possibilita maior controle do robô. Dentre outras funcionalidades, uma das principais é a poder girar os elementos do robô, o seu corpo, o radar e arma independentemente. 

Com essa independência pode-se utilizar alguns algoritmos para se movimentar com o objetivo de desviar das balas dos oponentes e atirar num tempo específico. 

Este código utiliza o movimento básico de wave surfing. Esse algoritmo de movimento foi base para a estratégia de movimentação de alguns robôs vencedores de campeonatos. Ele "surfa" nas ondas que são geradas a cada disparo efetuado pelo inimigo. Além disso, ele faz um acompanhamento da localização do meu robô e do robo inimigo, captura também o último nível de energia do inimigo e guarda as ondas geradas. 

Com esse algoritmo, o robô têm uma base inicial de dois dos principais componentes que um robô precisa que é o radar e o movimento. 

Para completar essa base inicial, foi utilizado nesse robô o algoritmo basico de GuessFactor targeting. Essa técnica têm por princípio acompanhar a movimentação do robô inimigo, e tentar encontrar um padrão de movimento e utilizar esses dados para atirar cada vez mais no alvo.

Em resumo, com esse robô eu aprendi o básico do robocode e as possibilidades que o desenvolvedor têm de criar um robô cada vez melhor. Utilizando os seus dados e dos oponentes pelos métodos disponíveis na classe AdvancedRobot.

## Melhorias futuras
- [x] Melhorar a movimentação do robô ao se aproximar de outros robôs.
- [x] Melhorar a precisão dos tiros. Porque o robô está gastando muita energia com tiros que não acertam o inimigo.